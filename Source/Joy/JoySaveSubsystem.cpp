// Fill out your copyright notice in the Description page of Project Settings.


#include "JoySaveSubsystem.h"
#include "JoySaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "InventoryItem.h"

#define SAVE_SLOT TEXT("joy_sav")

UJoySaveGame* UJoySaveSubsystem::LoadSave()
{
	USaveGame* LoadedSave = UGameplayStatics::LoadGameFromSlot(SAVE_SLOT, 0);
	
	JoySaveGame = Cast<UJoySaveGame>(LoadedSave);

	if (JoySaveGame)
	{
		return JoySaveGame;
	}
	//Failed to load saved game

	USaveGame* CreatedSave = UGameplayStatics::CreateSaveGameObject(UJoySaveGame::StaticClass());

	JoySaveGame = Cast<UJoySaveGame>(CreatedSave);

	if (JoySaveGame)
	{
		const bool bSaved = UGameplayStatics::SaveGameToSlot(JoySaveGame, SAVE_SLOT, 0);

		if (!bSaved)
		{
			GEngine->AddOnScreenDebugMessage(-1, 4.0F, FColor::Red, TEXT("Failed to write save object to disk."));
		}

		return JoySaveGame;
	}

	GEngine->AddOnScreenDebugMessage(-2, 4.0F, FColor::Red, TEXT("Failed to do anything with save."));

	JoySaveGame = nullptr;
	return JoySaveGame;
}

bool UJoySaveSubsystem::SaveGame(const TArray<UInventoryItem*>& InventoryItems)
{
	if (!IsValid(JoySaveGame)) return false;

	TArray<TSubclassOf<UInventoryItem>> ItemClasses;
	ItemClasses.Reserve(InventoryItems.Num());

	for (const UInventoryItem* item : InventoryItems)
	{
		ItemClasses.Add(item->GetClass());
	}

	JoySaveGame->InventoryItems = MoveTempIfPossible(ItemClasses);

	const bool bSaved = UGameplayStatics::SaveGameToSlot(JoySaveGame, SAVE_SLOT, 0);

	return bSaved;
}