// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JoyHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDamaged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHealed);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JOY_API UJoyHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UJoyHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Joy|Health")
	void Damage(float Amount);

	UFUNCTION(BlueprintCallable, Category = "Joy|Health")
	void Heal(float Amount);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Joy|Health")
	float MaxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "Joy|Health")
	float Health;

	UPROPERTY(BlueprintAssignable, Category = "Joy|Health")
	FOnDeath OnDeathDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Joy|Health")
	FOnDamaged OnDamagedDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Joy|Health")
	FOnHealed OnHealedDelegate;

	UPROPERTY(BlueprintReadOnly, Category = "Joy|Health")
	bool bIsDead;

private:
	UFUNCTION()
	void OnTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

	UFUNCTION()
	void OnTakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser);
};
