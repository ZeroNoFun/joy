// Fill out your copyright notice in the Description page of Project Settings.


#include "JoyTurret.h"
#include "Perception/PawnSensingComponent.h"

// Sets default values
AJoyTurret::AJoyTurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TurretMesh"));
	SetRootComponent(TurretMesh);

	PawnSensing = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensing"));
}

// Called when the game starts or when spawned
void AJoyTurret::BeginPlay()
{
	Super::BeginPlay();
	
	PawnSensing->OnSeePawn.AddDynamic(this, &AJoyTurret::OnSeePawn);
}

// Called every frame
void AJoyTurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AJoyTurret::TargetLost()
{
	bHasTarget = false;
	Target = nullptr;
}

void AJoyTurret::OnSeePawn(APawn* Pawn)
{
	//We should check for pawn class in here using IsA or Cast. But VR template pawn is pure blueprint so there is no good way to reference class of this pawn.
	//But this should do the work, since we are the only pawn in game (for now)

	bHasTarget = true;
	Target = Pawn;

	const float Rate = PawnSensing->SensingInterval + 0.1F;
	GetWorldTimerManager().SetTimer(TargetLostTimer, this, &AJoyTurret::TargetLost, Rate, false);
}