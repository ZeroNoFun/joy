// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JoyInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnItemAdded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnItemRemoved);


class UInventoryItem;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JOY_API UJoyInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UJoyInventoryComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Joy|Inventory")
	FOnItemAdded OnItemAddedDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Joy|Inventory")
	FOnItemRemoved OnItemRemovedDelegate;

	UFUNCTION(BlueprintCallable, Category = "Joy|Inventory")
	void AddInventoryItem(TSubclassOf<UInventoryItem> ItemClass);

	UFUNCTION(BlueprintCallable, Category = "Joy|Inventory")
	void RemoveInventoryItem(UInventoryItem* ItemToRemove);

	UFUNCTION(BlueprintCallable, Category = "Joy|Inventory")
	void ClearInventory();

	UFUNCTION(BlueprintPure, Category = "Joy|Inventory")
	const TArray<UInventoryItem*>& GetInventoryItems() const;

	UFUNCTION(BlueprintPure, Category = "Joy|Inventory")
	int32 GetItemsCount() const;

private:
	UPROPERTY(BlueprintReadOnly, Category = "Joy|Inventory", meta = (AllowPrivateAccess = "true"))
	TArray<UInventoryItem*> Items;
};
