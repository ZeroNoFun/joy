// Fill out your copyright notice in the Description page of Project Settings.


#include "JoyGrabComponent.h"
#include "Components/PrimitiveComponent.h"
#include "MotionControllerComponent.h"
#include "JoyVrInteractInterface.h"
#include "Kismet/GameplayStatics.h"
#include "Haptics/HapticFeedbackEffect_Base.h"

// Sets default values for this component's properties
UJoyGrabComponent::UJoyGrabComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UJoyGrabComponent::BeginPlay()
{
	Super::BeginPlay();

	// Doing this once in BeginPlay to remember if we should enable physics after drop
	if (UPrimitiveComponent* Comp = Cast<UPrimitiveComponent>(GetAttachParent()))
	{
		bSimulateOnDrop = Comp->IsAnySimulatingPhysics();
	}
}


// Called every frame
void UJoyGrabComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UJoyGrabComponent::TryGrab(UMotionControllerComponent* MotionController)
{
	switch (GrabType)
	{
	case EGrabType::Free:
		{
			SetPrimitiveComponentPhysics(false);
			AttachParentToMotionController(MotionController);
			ActiveController = MotionController;
			bIsHeld = true;
			break;
		}
	case EGrabType::Snap:
		{
			SetPrimitiveComponentPhysics(false);
			AttachParentToMotionController(MotionController);
			ActiveController = MotionController;
			bIsHeld = true;

			USceneComponent* Parent = GetAttachParent();

			Parent->SetRelativeRotation(GetRelativeRotation().GetInverse(), false, nullptr, ETeleportType::TeleportPhysics);

			const FVector Dir = (GetComponentLocation() - Parent->GetComponentLocation()) * -1;
			const FVector Location = ActiveController->GetComponentLocation() + Dir;

			Parent->SetWorldLocation(Location);
			break;
		}
	default:
		bIsHeld = false;
	}

	if (!bIsHeld) return false;

	OnGrabObjectDelegate.Broadcast();

	/*
	APlayerController* Controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (IsValid(Controller) && IsValid(OnGrabHapticEffect))
	{
		const EControllerHand Hand = ActiveController->MotionSource == FName("Left") ? EControllerHand::Left : EControllerHand::Right;
		Controller->PlayHapticEffect(OnGrabHapticEffect, Hand);
	}
	*/

	return true;
}

bool UJoyGrabComponent::TryRelease()
{
	switch (GrabType)
	{
	case EGrabType::Free:
	case EGrabType::Snap:
		{
			if (bSimulateOnDrop)
			{
				SetPrimitiveComponentPhysics(true);
			}
			else
			{
				GetOwner()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);			
			}
			bIsHeld = false;
			break;
		}
	default:
		{
			bIsHeld = false;
			break;
		}
	}

	//Failed to release
	if (bIsHeld) return false;
	
	OnDropObjectDelegate.Broadcast();

	return true;
}

void UJoyGrabComponent::TriggerPressed()
{
	if (GetOwner()->GetClass()->ImplementsInterface(UJoyVrInteractInterface::StaticClass()))
	{
		IJoyVrInteractInterface::Execute_TriggerPressed(GetOwner());
	}
}

void UJoyGrabComponent::TriggerReleased()
{
	if (GetOwner()->GetClass()->ImplementsInterface(UJoyVrInteractInterface::StaticClass()))
	{
		IJoyVrInteractInterface::Execute_TriggerReleased(GetOwner());
	}
}

void UJoyGrabComponent::TriggerAxis(float Value)
{
	if (GetOwner()->GetClass()->ImplementsInterface(UJoyVrInteractInterface::StaticClass()))
	{
		IJoyVrInteractInterface::Execute_TriggerAxis(GetOwner(), Value);
	}
}

void UJoyGrabComponent::OculusFirstButton()
{
	if (GetOwner()->GetClass()->ImplementsInterface(UJoyVrInteractInterface::StaticClass()))
	{
		IJoyVrInteractInterface::Execute_OculusFirstButton(GetOwner());
	}
}

void UJoyGrabComponent::SetPrimitiveComponentPhysics(const bool bShouldSimulate)
{
	if (UPrimitiveComponent* Comp = Cast<UPrimitiveComponent>(GetAttachParent()))
	{
		Comp->SetSimulatePhysics(bShouldSimulate);
	}
}

void UJoyGrabComponent::AttachParentToMotionController(UMotionControllerComponent* MotionController)
{
	GetAttachParent()->AttachToComponent(MotionController, FAttachmentTransformRules::KeepWorldTransform);
}
