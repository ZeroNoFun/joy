// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "JoyGrabComponent.generated.h"

UENUM()
enum EGrabType
{
	None	UMETA(DisplayName = "None"),
	Free	UMETA(DisplayName = "Free"),
	Snap	UMETA(DisplayName = "Snap"),
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGrabObject);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDropObject);

class UMotionControllerComponent;


//Basically GrabComponent but written in C++
UCLASS( Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JOY_API UJoyGrabComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UJoyGrabComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UHapticFeedbackEffect_Base* OnGrabHapticEffect;

	UPROPERTY(BlueprintReadOnly)
	bool bIsHeld;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TEnumAsByte<EGrabType> GrabType;

	UPROPERTY(BlueprintAssignable)
	FOnGrabObject OnGrabObjectDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnDropObject OnDropObjectDelegate;

	UPROPERTY(BlueprintReadOnly, Category = "Joy|Grab")
	UMotionControllerComponent* ActiveController;

	UFUNCTION(BlueprintCallable, Category = "Joy|Grab")
	bool TryGrab(UMotionControllerComponent* MotionController);

	UFUNCTION(BlueprintCallable, Category = "Joy|Grab")
	bool TryRelease();

	UFUNCTION(BlueprintCallable, Category = "Joy|Grab")
	void TriggerPressed();

	UFUNCTION(BlueprintCallable, Category = "Joy|Grab")
	void TriggerReleased();

	UFUNCTION(BlueprintCallable, Category = "Joy|Grab")
	void TriggerAxis(float Value);

	UFUNCTION(BlueprintCallable, Category = "Joy|Grab")
	void OculusFirstButton();
	
private:
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bSimulateOnDrop;

	void SetPrimitiveComponentPhysics(const bool bShouldSimulate);

	void AttachParentToMotionController(UMotionControllerComponent* MotionController);
};
