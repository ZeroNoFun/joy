// Fill out your copyright notice in the Description page of Project Settings.


#include "JoyHealthComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/DamageType.h"
#include "Components/PrimitiveComponent.h"


// Sets default values for this component's properties
UJoyHealthComponent::UJoyHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	MaxHealth = 100;
}


// Called when the game starts
void UJoyHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	AActor* Owner = GetOwner();
	if (IsValid(Owner))
	{
		Owner->OnTakePointDamage.AddDynamic(this, &UJoyHealthComponent::OnTakePointDamage);
		Owner->OnTakeRadialDamage.AddDynamic(this, &UJoyHealthComponent::OnTakeRadialDamage);
	}
}


// Called every frame
void UJoyHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UJoyHealthComponent::Heal(float Amount)
{
	if (Amount <= 0 || bIsDead) return;

	const float Healed = Health + Amount;
	Health = FMath::Clamp<float>(Healed, Health, MaxHealth);

	OnHealedDelegate.Broadcast();
}

void UJoyHealthComponent::Damage(float Amount)
{
	if (Amount <= 0 || bIsDead) return;

	const float Damaged = Health - Amount;

	if (Damaged <= 0)
	{
		Health = 0;
		bIsDead = true;
		OnDeathDelegate.Broadcast();
	}
	else
	{
		Health = Damaged;
		OnDamagedDelegate.Broadcast();
	}
}

void UJoyHealthComponent::OnTakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
	//Ignoring all information about damage for now

	this->Damage(Damage);
}

void UJoyHealthComponent::OnTakeRadialDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin, FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
	//Ignoring all information about damage for now

	this->Damage(Damage);
}