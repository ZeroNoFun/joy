// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "InventoryItem.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, Abstract)
class JOY_API UInventoryItem : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Joy|Item")
	FText ItemName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Joy|Item")
	TSoftObjectPtr<class UTexture2D> ItemTexture;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Joy|Item")
	void UseItem(AActor* User);
};
