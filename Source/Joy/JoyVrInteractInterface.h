// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "JoyVrInteractInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UJoyVrInteractInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Basically the same interface as in VRTemplate, but in c++
 */
class JOY_API IJoyVrInteractInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Trigger")
	void TriggerAxis(float Value);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Trigger")
	void TriggerPressed();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Trigger")
	void TriggerReleased();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Trigger")
	void SecondaryTriggerAxis(float Value);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Trigger")
	void SecondaryTriggerPressed();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Trigger")
	void SecondaryTriggerReleased();

	//I'm Not sure how to name this button :)
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Joy|Vr|Button")
	void OculusFirstButton();
};
