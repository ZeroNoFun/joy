// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "JoyTurret.generated.h"

UCLASS()
class JOY_API AJoyTurret : public APawn
{
	GENERATED_BODY()
	
public:	
	AJoyTurret();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UPawnSensingComponent* PawnSensing;

public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UStaticMeshComponent* TurretMesh;

	UFUNCTION(BlueprintCallable, Category = "Joy|Turret")
	void TargetLost();

private:
	UFUNCTION()
	void OnSeePawn(APawn* Pawn);

	UPROPERTY(BlueprintReadOnly, Category = "Joy|Turret", meta = (AllowPrivateAccess = "true"))
	bool bHasTarget;

	UPROPERTY(BlueprintReadOnly, Category = "Joy|Turret", meta = (AllowPrivateAccess = "true"))
	APawn* Target;

	UPROPERTY(BlueprintReadOnly, Category = "Joy|Turret", meta = (AllowPrivateAccess = "true"))
	FTimerHandle TargetLostTimer;
};
