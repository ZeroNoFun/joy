// Fill out your copyright notice in the Description page of Project Settings.


#include "JoyInventoryComponent.h"
#include "InventoryItem.h"
#include "Kismet/GameplayStatics.h"
#include "JoySaveSubsystem.h"
#include "JoySaveGame.h"

// Sets default values for this component's properties
UJoyInventoryComponent::UJoyInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UJoyInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	//Load from save

	const UGameInstance* GameInstance = UGameplayStatics::GetGameInstance(this);
	if (!GameInstance) return;

	UJoySaveSubsystem* JoySaveSubsystem = GameInstance->GetSubsystem<UJoySaveSubsystem>();
	if (!JoySaveSubsystem) return;

	UJoySaveGame* JoySaveGame = JoySaveSubsystem->GetJoySaveGame();

	Items.Reserve(JoySaveGame->InventoryItems.Num());

	for (TSubclassOf<UInventoryItem> ItemClass : JoySaveGame->InventoryItems)
	{
		Items.Add(NewObject<UInventoryItem>(this, ItemClass));
	}
}

void UJoyInventoryComponent::AddInventoryItem(TSubclassOf<UInventoryItem> ItemClass)
{
	Items.Add(NewObject<UInventoryItem>(this, ItemClass));

	OnItemAddedDelegate.Broadcast();
}

void UJoyInventoryComponent::RemoveInventoryItem(UInventoryItem* ItemToRemove)
{
	Items.Remove(ItemToRemove);

	OnItemRemovedDelegate.Broadcast();
}

void UJoyInventoryComponent::ClearInventory()
{
	Items.Empty();

	OnItemRemovedDelegate.Broadcast();
}

const TArray<UInventoryItem*>& UJoyInventoryComponent::GetInventoryItems() const
{
	return Items;
}

int32 UJoyInventoryComponent::GetItemsCount() const
{
	return Items.Num();
}


// Called every frame
void UJoyInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
