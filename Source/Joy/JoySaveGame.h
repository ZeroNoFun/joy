// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "JoySaveGame.generated.h"

class UInventoryItem;

/**
 * 
 */
UCLASS()
class JOY_API UJoySaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Joy|Save")
	TArray<TSubclassOf<UInventoryItem>> InventoryItems;
	
};
