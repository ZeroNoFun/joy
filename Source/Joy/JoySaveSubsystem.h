// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "JoySaveSubsystem.generated.h"

class UJoySaveGame;
class UInventoryItem;

/**
 * 
 */
UCLASS()
class JOY_API UJoySaveSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Joy|Subsystem|Save")
	UJoySaveGame* LoadSave();

	UFUNCTION(BlueprintCallable, Category = "Joy|Subsystem|Save")
	bool SaveGame(const TArray<UInventoryItem*>& InventoryItems);

	UFUNCTION(BlueprintPure, Category = "Joy|Subsystem|Save")
	FORCEINLINE UJoySaveGame* GetJoySaveGame() { return JoySaveGame; }

private:
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Joy|Subsystem|Save")
	UJoySaveGame* JoySaveGame;
};
